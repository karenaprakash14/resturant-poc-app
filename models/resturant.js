const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const resturantSchema = new Schema({
    name: {
        type: String,
        required: [true],
        unique: 1
    },
    address: {
        type: String,
        required: [true, 'Resturant address required'],
    },
    image: {
        type: String,
        required: false,
    },
    adminId: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: true
    }
},
    { timestamps: true });

resturantSchema.index({ '$**': 'text' });

// custom error if Resturant Name already used
resturantSchema.post('save', function (error, doc, next) {
    if (error.name === 'MongoError' && error.code === 11000) {
        next(new Error('Resturant name already used.'));
    } else {
        console.log('error', error)
        next(error);
    }
});
module.exports = mongoose.model('resturant', resturantSchema);