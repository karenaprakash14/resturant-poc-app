const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const reviewSchema = new Schema({
    reviewerId: {
        type: Schema.Types.ObjectId,
        ref: 'user',
        required: [true],
        unique: 1
    },
    review: {
        type: String,
        required: [true, 'Review is required'],
    },
    adminReplay: {
        type: String,
        required: false,
    },
    rating: {
        type: Number,
        required: [true, 'Rating is required'],
    },
    resturantId: {
        type: Schema.Types.ObjectId,
        ref: 'resturant',
        required: true
    }
},
    { timestamps: true });

// custom error if Resturant Name already used
reviewSchema.post('save', function (error, doc, next) {
    if (error.name === 'MongoError' && error.code === 11000) {
        console.log('error.message', error.message)
        next(new Error(error.message));
    } else {
        next(error);
    }
});

module.exports = mongoose.model('review', reviewSchema);