const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
require('dotenv').config();
app.use(cors());
app.use(bodyParser.json());

//database connection
const connectDB = require('./db/connections');
connectDB();

// static folder 
app.use('/resturant',express.static('public/resturant'));

app.use('/api/', require('./routes/user').router);
app.use('/api/', require('./routes/resturant').router);
app.use('/api/', require('./routes/review').router);

// route not found
app.use('*', function (req, res, next) {
    next(new Error('Route not found.'))
})

//global error handler
app.use((err, req, res, next) => {
    res.status(err.status || 402).json({
        success: 0,
        message: err.message && err.message || 'Error while making request'
    })
});

//port 
const Port = process.env.PORT || 3030;

app.listen(Port, () => {
    console.log(`server is running on port ${Port}`);
})


