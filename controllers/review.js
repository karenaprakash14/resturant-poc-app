
const { body } = require('express-validator');
const mongoose = require('mongoose');
const ReviewModal = require('../models/review'); // User Modal

/**
 * addOrEditReview : /api/add-edit-review
 */

// addOrEditReview : add or edit review
module.exports.addOrEditReview = async (req, res) => {
    try {
        // admin can reply on review on existing review 
        if (req.body.id) {
            const isAdminUser = req.user.role === 'admin';

            if (isAdminUser) {
                // check replay message is there or not 
                if (!req.body.adminReplay) throw new Error('Admin reply is required.');
            }

            // save admin replay
            const reviewDetails = await ReviewModal.findById(req.body.id);
            if (!reviewDetails) throw new Error('Review not found.');
            if (isAdminUser) {
                reviewDetails.adminReplay = req.body.adminReplay;
            } else {
                // edit review details if end-user
                reviewDetails.reviewerId = req.user._id;
                reviewDetails.review = req.body.review;
                reviewDetails.rating = req.body.rating;
                reviewDetails.resturantId = req.body.resturantId;
            }
            await reviewDetails.save();
            res.status(200).json({
                success: 1,
                message: 'Review updated successfully.',
                id: reviewDetails._id
            })
        } else {
            // reviewr can review 
            // add / edit logic
            const request = {
                reviewerId: req.user._id,
                review: req.body.review,
                rating: req.body.rating,
                resturantId: req.body.resturantId
            };
            // add request
            const reviewDetails = new ReviewModal(request);
            await reviewDetails.save();
            res.status(200).json({
                success: 1,
                message: 'Review added successfully.',
                id: reviewDetails._id
            })
        }
    } catch (error) {
        res.status(error.status || 500).json({
            success: 0,
            message: error.message
        })
    }
}

// getReviewsResturantWise : get reviews resturantwise
module.exports.getReviewsResturantWise = async (req, res) => {
    try {

        const { resturantId, all, limit, page, search } = req.body;
        const perPage = limit;

        let query = {
            resturantId
        };

        let paginate = {};

        // if search provided 
        if (search) {
            query = { $text: { $search: search } }
        }

        // if not all request then set limit and skip for pagination 
        if (!all) {
            if (limit && limit > 0) {
                paginate.limit = perPage;
            }
            if (page && page > 0) {
                paginate.skip = perPage * (page - 1);
            }
        }

        const resturants = await ReviewModal.
            find(query, {}, paginate)
            .sort('-createdAt');

        const totalCount = await ReviewModal.
            countDocuments(query);

        // aggregation: for rating details
        const aggregation = [
            { $match: { resturantId: mongoose.Types.ObjectId(resturantId) } },
            {
                $group: { _id: "$resturantId", total_rating: { $sum: "$rating" }, avg_rating: { $avg: "$rating" }, max_rating: { $max: "$rating" }, min_rating: { $min: "$rating" } }
            },
            { $project: { data: 0 } },
        ];

        const result = await ReviewModal
            .aggregate(aggregation)

        const ratings = {
            total_rating: null,
            avg_rating: null,
            max_rating: null,
            min_rating: null,
        }

        const ratingData = result && result[0] ? result[0] : null;
        if (result && result[0]) {
            ratings.total_rating = ratingData.total_rating;
            ratings.avg_rating = ratingData.avg_rating;
            ratings.max_rating = ratingData.max_rating;
            ratings.min_rating = ratingData.min_rating;
        }

        res.status(200).json({
            status: true,
            message: "Reviews fetched successfully",
            resturants,
            totalCount,
            ratingData
        })
    } catch (error) {
        res.status(error.status || 500).json({
            success: 0,
            message: error.message
        })
    }
}