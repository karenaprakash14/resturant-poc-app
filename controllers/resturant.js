
const { body } = require('express-validator');
const mongoose = require('mongoose');
const ResturantModal = require('../models/resturant'); // User Modal

/**
 * addOrEditResturant : /api/add-edit-resturant
 */

// addOrEditResturant Validation 
module.exports.addOrEditResturantValidation = [
    body('name').not().isEmpty(),
    body('address').not().isEmpty(),
];

// addOrEditResturant : add or edit resturant
module.exports.addOrEditResturant = async (req, res) => {
    try {
        if (req.user.role !== 'admin') throw new Error('User not authorized.');

        const posterUrl = req.file !== undefined && req.file ? `/resturant/${req.file.filename}` : null;

        // edit request
        if (req.body.id) {
            const resturantId = mongoose.Types.ObjectId(req.body.id);
            const resturantDetails = await ResturantModal.findById(resturantId);
            resturantDetails.name = req.body.name;
            resturantDetails.address = req.body.address;
            if (posterUrl) resturantDetails.poster = posterUrl;
            await resturantDetails.save();
            res.status(200).json({
                success: 1,
                message: 'Resturant updated successfully.',
                id: resturantDetails._id
            })
        } else {
            const request = {
                name: req.body.name,
                address: req.body.address,
                adminId: req.user._id,
                poster: posterUrl,
            };

            // add request
            const resturantDetails = new ResturantModal(request);
            await resturantDetails.save();
            res.status(200).json({
                success: 1,
                message: 'Resturant added successfully.',
                id: resturantDetails._id
            })
        }

    } catch (error) {
        res.status(error.status || 500).json({
            success: 0,
            message: error.message
        })
    }
}

// getAllResturants : get all resturants with pagination / searching / filter 
module.exports.getAllResturants = async (req, res) => {
    try {

        if (req.user.role !== 'admin') throw new Error('User not authorized.');

        const { all, limit, page, search } = req.body;
        const perPage = limit;
        let query = {};
        let paginate = {};

        // if search provided 
        if (search) {
            query = { $text: { $search: search } }
        }

        // if not all request then set limit and skip for pagination 
        if (!all) {
            if (limit && limit > 0) {
                paginate.limit = perPage;
            }
            if (page && page > 0) {
                paginate.skip = perPage * (page - 1);
            }
        }

        const resturants = await ResturantModal.
            find(query, {}, paginate)
            .sort('-createdAt');

        const totalCount = await ResturantModal.
            countDocuments(query);

        res.status(200).json({
            status: true,
            message: "Resturants fetched successfully",
            resturants,
            totalCount
        })
    } catch (error) {
        res.status(error.status || 500).json({
            success: 0,
            message: error.message
        })
    }
}

// getAllResturantsWithRating : get all resturants with rating
module.exports.getAllResturantsWithRating = async (req, res) => {
    try {

        const { all, limit = 10, page = 1 } = req.body;
        const perPage = limit;
        const skip = perPage * (page - 1);

        const aggregation = [{
            $lookup: {
                from: "reviews",
                let: { resturantId: '$_id' },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $and: [
                                    { $eq: ['$resturantId', '$$resturantId'] },
                                ]
                            }
                        }
                    },
                    {
                        $group: { _id: "$resturantId", total_rating: { $sum: "$rating" }, avg_rating: { $avg: "$rating" }, max_rating: { $max: "$rating" }, min_rating: { $min: "$rating" } }
                    },
                ],
                as: "data"
            }
        }, {
            $replaceRoot: { newRoot: { $mergeObjects: [{ $arrayElemAt: ["$data", 0] }, "$$ROOT"] } }
        },
        { $project: { data: 0 } },
        { $sort: { avg_rating: -1 } },
        ];

        if (!all) {
            aggregation.push({
                '$facet': {
                    metadata: [{ $count: "total" }, { $addFields: { page } }],
                    data: [{ $skip: skip }, { $limit: perPage }]
                }
            })
        } else {
            aggregation.push({
                '$facet': {
                    metadata: [{ $count: "total" }, { $addFields: { page } }],
                    data: []
                }
            })
        }

        const result = await ResturantModal
            .aggregate(aggregation)

        res.status(200).json({
            status: true,
            message: "Resturants fetched successfully",
            result
        })

    } catch (error) {
        res.status(error.status || 500).json({
            success: 0,
            message: error.message
        })
    }
}

// getResturantDetail : get resturant detail
module.exports.getResturantDetail = async (req, res) => {
    try {

        if (req.user.role !== 'admin') throw new Error('User not authorized.');

        const {resturantId} = req.params;
        console.log('resturantId',resturantId)
        if(!resturantId) throw new Error('Resturant id is required.');
        const resturant = await ResturantModal.
            findById(resturantId)

        res.status(200).json({
            status: true,
            message: "Resturant fetched successfully",
            resturant,
        })
    } catch (error) {
        res.status(error.status || 500).json({
            success: 0,
            message: error.message
        })
    }
}

// deleteResturant : delete resturant
module.exports.deleteResturant = async (req, res) => {
    try {

        if (req.user.role !== 'admin') throw new Error('User not authorized.');

        const {resturantId} = req.params;
        if(!resturantId) throw new Error('Resturant id is required.');
        const resturant = await ResturantModal.
            findByIdAndRemove(resturantId)

        res.status(200).json({
            status: true,
            message: "Resturant delete successfully",
            resturant,
        })
    } catch (error) {
        res.status(error.status || 500).json({
            success: 0,
            message: error.message
        })
    }
}
