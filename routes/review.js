const express = require('express');
const router = express.Router();
// controllers
const reviewController = require('../controllers/review');
const authController = require('../controllers/auth');

router.post('/add-edit-review', authController.auth, reviewController.addOrEditReview);

router.post('/get-reviews-resturant-wise', authController.auth, reviewController.getReviewsResturantWise);

module.exports = {
    router
}