const express = require('express');
const multer = require('multer');
const router = express.Router();
// controllers
const resturantController = require('../controllers/resturant');
const authController = require('../controllers/auth');


/**
 * @route POST /api/add-edit-resturant
 * @param {User.model} user.body.required
 * @returns {UserResponse.model} 200 - Create successfully
 * @returns {object} 200 - Validation Error
 */

//storage configuration for user_image
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/resturant');
    },
    filename: function (req, file, cb) { // filename of resturant 
        // cb(null, `${Date.now()}.${file.originalname.split('.')[1]}`);
        cb(null,`${req.body.name}.${file.originalname.split('.')[1]}`);
    }
});

const uploadImage = multer({ storage: storage });

router.post('/add-edit-resturant',authController.auth,uploadImage.single('poster'), resturantController.addOrEditResturantValidation, resturantController.addOrEditResturant);

router.post('/get-resturants', authController.auth, resturantController.getAllResturants);

router.post('/get-resturants-with-rating', authController.auth, resturantController.getAllResturantsWithRating);

router.get('/get-resturant-detail/:resturantId', authController.auth, resturantController.getResturantDetail);

router.post('/delete-resturant/:resturantId', authController.auth, resturantController.deleteResturant);

module.exports = {
    router
}